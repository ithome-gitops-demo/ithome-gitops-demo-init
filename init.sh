#!/bin/bash

infra_project_id=

gitlab_username=
gitlab_access_token=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=





# install 

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner -y

sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible -y

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update
sudo apt-get install terraform -y

ssh-keygen -f tmp-cloud9 -N ''

sudo mv tmp-cloud9 /home/ubuntu/
sudo mv tmp-cloud9.pub /home/ubuntu/
sudo chmod 600 /home/ubuntu/tmp-cloud9
sudo chmod 600 /home/ubuntu/tmp-cloud9.pub

cp ansible.cfg /etc/ansible/ansible.cfg

curl --request POST --header "PRIVATE-TOKEN: $gitlab_access_token" "https://gitlab.com/api/v4/projects/$infra_project_id/variables" \
--form "key=tf_name" \
--form "value=$gitlab_access_token" \
--form "protected=true" \
--form "masked=true"

curl --request POST --header "PRIVATE-TOKEN: $gitlab_access_token" "https://gitlab.com/api/v4/projects/$infra_project_id/variables" \
--form "key=tf_pass" \
--form "value=$gitlab_access_token" \
--form "protected=true" \
--form "masked=true"

curl --request POST --header "PRIVATE-TOKEN: $gitlab_access_token" "https://gitlab.com/api/v4/projects/$infra_project_id/variables" \
--form "key=AWS_ACCESS_KEY_ID" \
--form "value=$AWS_ACCESS_KEY_ID" \
--form "protected=true" \
--form "masked=true"

curl --request POST --header "PRIVATE-TOKEN: $gitlab_access_token" "https://gitlab.com/api/v4/projects/$infra_project_id/variables" \
--form "key=AWS_SECRET_ACCESS_KEY" \
--form "value=$AWS_SECRET_ACCESS_KEY" \
--form "protected=true" \
--form "masked=true"

echo "no" > /tmp/app.version.old

gitlab-runner register
